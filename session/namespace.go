// Copyright (c) 2015 OSD. All rights reserved.
// This Source Code Form is subject to the terms of the BSD 2-Clause license.
// If a copy of the BSD license was not distributed with this file, you can
// obtain one at http://opensource.org/licenses/BSD-2-Clause.

package session

type Namespace struct {
	ns string
	c  Container
}

func (s *Session) Namespace(ns string) *Namespace {
	return &Namespace{ns: ns, c: s.Container}
}

func (n *Namespace) Get(key string) interface{} {
	k := ContainerKey{Namespace: n.ns, Key: key}
	return n.c[k]
}

func (n *Namespace) Set(key string, value interface{}) {
	k := ContainerKey{Namespace: n.ns, Key: key}
	n.c[k] = value
}

func (n *Namespace) Del(key string) {
	k := ContainerKey{Namespace: n.ns, Key: key}
	delete(n.c, k)
}
