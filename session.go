// Copyright (c) 2015 OSD. All rights reserved.
// This Source Code Form is subject to the terms of the BSD 2-Clause license.
// If a copy of the BSD license was not distributed with this file, you can
// obtain one at http://opensource.org/licenses/BSD-2-Clause.

package gingaesession

import (
	"bitbucket.org/osddk/gin-gae-session/session"
	"bitbucket.org/osddk/gin-gae-session/storage"
	"errors"
	"github.com/gin-gonic/gin"
	"google.golang.org/appengine"
	"google.golang.org/appengine/log"
	"strings"
)

func SessionHandler(name string) gin.HandlerFunc {
	return func(c *gin.Context) {
		// Initialize session.
		gae := appengine.NewContext(c.Request)
		ds := storage.NewDatastoreStorage(gae)
		s := session.NewSession(name, ds)
		c.Set("session", s)

		// Get session id from authorization header as per RFC6750.
		if h := c.Request.Header.Get("Authorization"); h != "" {
			// Split header and check for bearer token.
			a := strings.SplitN(h, " ", 2)
			if len(a) != 2 || a[0] != "Bearer" || a[1] == "" {
				log.Criticalf(gae, "Invalid authorizaton header: %s", h)
				c.AbortWithError(400, errors.New("Invalid authorizaton header"))
				return
			}

			// We got a bearer token. Extract and load session.
			s.Id = a[1]
			if err := s.Load(); err != nil {
				// Failed loading session. Warn and clear id.
				log.Warningf(gae, "Error loading session with id '%s': %s", s.Id, err)
				s.Id = ""
			}
		}

		// Call next handler. We'll return here on the way out.
		c.Next()

		// Save session if we have a session id.
		if s.Id != "" {
			if err := s.Save(); err != nil {
				log.Warningf(gae, "Error saving session with id '%s': %s", s.Id, err)
			}
		}
	}
}
