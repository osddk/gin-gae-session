// Copyright (c) 2015 OSD. All rights reserved.
// This Source Code Form is subject to the terms of the BSD 2-Clause license.
// If a copy of the BSD license was not distributed with this file, you can
// obtain one at http://opensource.org/licenses/BSD-2-Clause.

package storage

import (
	"bitbucket.org/osddk/gin-gae-session/session"
	"bytes"
	"encoding/gob"
	"errors"
	"golang.org/x/net/context"
	"google.golang.org/appengine/datastore"
	"time"
)

const (
	EntityKind = "Session"
)

var (
	ErrNoSessionId     = errors.New("Session id not set")
	ErrSessionExpired  = errors.New("Session expired")
	ErrSessionNotFound = errors.New("Session not found")
)

type Entity struct {
	Expire time.Time
	Data   []byte
}

type DatastoreStorage struct {
	c context.Context
}

func NewDatastoreStorage(c context.Context) *DatastoreStorage {
	return &DatastoreStorage{c: c}
}

func (ds *DatastoreStorage) Load(s *session.Session) error {
	// We need a session id to load the session.
	if s.Id == "" {
		return ErrNoSessionId
	}

	// Fetch session data from datastore.
	e := &Entity{}
	k := datastore.NewKey(ds.c, EntityKind, s.Id, 0, nil)
	if err := datastore.Get(ds.c, k, e); err != nil {
		if err == datastore.ErrNoSuchEntity {
			return ErrSessionNotFound
		}
		return err
	}

	// Check if session has expired and skip loading the data if it has.
	if e.Expire.Before(time.Now().UTC()) {
		return ErrSessionExpired
	}

	// Decode data and store in session container.
	m := s.Container
	buf := bytes.NewBuffer(e.Data)
	dec := gob.NewDecoder(buf)
	if err := dec.Decode(&m); err != nil {
		return err
	}
	return nil
}

func (ds *DatastoreStorage) Save(s *session.Session) error {
	// Check if we have a session id before storing.
	if s.Id == "" {
		return ErrNoSessionId
	}

	// Encode session data.
	buf := new(bytes.Buffer)
	enc := gob.NewEncoder(buf)
	if err := enc.Encode(s.Container); err != nil {
		return err
	}

	// Store encoded data with a timestamp in datastore.
	e := &Entity{
		Expire: time.Now().UTC().Add(s.MaxAge * time.Second),
		Data:   buf.Bytes(),
	}
	k := datastore.NewKey(ds.c, EntityKind, s.Id, 0, nil)
	if _, err := datastore.Put(ds.c, k, e); err != nil {
		return err
	}
	return nil
}
